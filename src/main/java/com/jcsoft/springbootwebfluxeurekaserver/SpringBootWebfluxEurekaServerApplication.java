package com.jcsoft.springbootwebfluxeurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Utilizaremos Eureka para manejar los microservicios creados anteriormente, será Eureka quien controle la ubicación
 * y estados de los microservicios. Al manejar Eureka los microservicios y tener control sobre los puertos, host y
 * nombres permite que se pueda lenvantar no solo una instancia de un servicio sino las que se desee.
 */
@EnableEurekaServer
@SpringBootApplication
public class SpringBootWebfluxEurekaServerApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(SpringBootWebfluxEurekaServerApplication.class, args);
    }

}
